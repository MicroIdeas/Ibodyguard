package com.innova.citylink.feedreader;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.innova.citylink.R;
import com.innova.citylink.feedreader.db.DbAdapter;
import com.innova.citylink.feedreader.model.Article;
import com.innova.citylink.feedreader.util.DateUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class ArticleDetailFragment extends Fragment {

	public static final String ARG_ITEM_ID = "item_id";

	Article displayedArticle;
	DbAdapter db;

	public ArticleDetailFragment() {
		setHasOptionsMenu(true); // this enables us to set actionbar from
									// fragment
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		db = new DbAdapter(getActivity());

		displayedArticle = (Article) getActivity().getIntent()
				.getSerializableExtra(Article.KEY);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_article_detail,
				container, false);
		if (displayedArticle != null) {
			String title = displayedArticle.getTitle();
			String pubDate = displayedArticle.getPubDate();
			SimpleDateFormat df = new SimpleDateFormat(
					"EEE, dd MMM yyyy kk:mm:ss Z", Locale.ENGLISH);
			try {
				Date pDate = df.parse(pubDate);
				pubDate = "Este post ha sido publicado "
						+ DateUtils.getDateDifference(pDate) + " por "
						+ displayedArticle.getAuthor();
			} catch (ParseException e) {
				Log.e("DATE PARSING", "Error parsing date..");
				pubDate = "publicado por " + displayedArticle.getAuthor();
			}

			String content = displayedArticle.getEncodedContent();
			((TextView) rootView.findViewById(R.id.article_title))
					.setText(title);
			((TextView) rootView.findViewById(R.id.article_author))
					.setText(pubDate);
			
			if(content == null) {
				content = displayedArticle.getDescription();
			}
			
			((TextView) rootView.findViewById(R.id.article_detail))
					.setText(Html.fromHtml(content));
		}
		return rootView;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.detailmenu, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		Log.d("item ID : ", "onOptionsItemSelected Item ID" + id);
		// if (id == R.id.actionbar_saveoffline) {
		// Toast.makeText(getActivity().getApplicationContext(),
		// "This article has been saved of offline reading.",
		// Toast.LENGTH_LONG).show();
		// return true;
		// } else

		if (id == R.id.actionbar_markunread) {
			db.openToWrite();
			db.markAsUnread(displayedArticle.getGuid());
			db.close();
			displayedArticle.setRead(false);

			Toast.makeText(getActivity().getApplicationContext(),
					"Este artículo fue marcado como no leído.",
					Toast.LENGTH_LONG).show();

			return true;
		} else {
			return super.onOptionsItemSelected(item);
		}
	}

	public static ArticleDetailFragment newInstance(Article article) {
		ArticleDetailFragment fragment = new ArticleDetailFragment();
		Bundle bundle = new Bundle();
		bundle.putSerializable(Article.KEY, article);
		fragment.setArguments(bundle);

		return fragment;
	}
}