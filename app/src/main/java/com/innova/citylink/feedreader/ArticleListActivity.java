package com.innova.citylink.feedreader;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import com.innova.citylink.R;
import com.innova.citylink.feedreader.adapter.ArticleListAdapter;
import com.innova.citylink.feedreader.db.DbAdapter;
import com.innova.citylink.feedreader.model.Article;

//TODO: esta clase se deberia eliminar
public class ArticleListActivity extends FragmentActivity {
	
	private DbAdapter dba;

	public ArticleListActivity() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_article_list);
		dba = new DbAdapter(this);

		if (findViewById(R.id.article_detail_container) != null) {
			((ArticleListFragment) getSupportFragmentManager()
					.findFragmentById(R.id.article_list))
					.setActivateOnItemClick(true);
		}
	}

	// @Override
	public void onItemSelected(String id) {
		Article selected = (Article) ((ArticleListFragment) getSupportFragmentManager()
				.findFragmentById(R.id.article_list)).getListAdapter().getItem(
				Integer.parseInt(id));

		// mark article as read
		dba.openToWrite();
		dba.markAsRead(selected.getGuid());
		dba.close();
		selected.setRead(true);
		ArticleListAdapter adapter = (ArticleListAdapter) ((ArticleListFragment) getSupportFragmentManager()
				.findFragmentById(R.id.article_list)).getListAdapter();
		adapter.notifyDataSetChanged();
		Log.e("CHANGE", "Changing to read: ");

		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		Fragment fragment = ArticleDetailFragment.newInstance(selected);

		ft.replace(R.id.article_list, fragment);
		ft.commit();
	}
}
