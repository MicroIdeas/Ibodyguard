package com.innova.citylink.feedreader;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.innova.citylink.R;
import com.innova.citylink.feedreader.adapter.ArticleListAdapter;
import com.innova.citylink.feedreader.db.DbAdapter;
import com.innova.citylink.feedreader.model.Article;
import com.innova.citylink.feedreader.service.RssService;

import java.util.ArrayList;
import java.util.List;

// TODO: controlar exception para cuando no hay red.
public class ArticleListFragment extends ListFragment {

	private static final String STATE_ACTIVATED_POSITION = "activated_position";
	private static final String BLOG_URL = "http://www.lavoz.com.ar/rss.xml";
	private int mActivatedPosition = ListView.INVALID_POSITION;
	private RssService rssService;
	private DbAdapter dba;

	public static ArticleListFragment newInstance() {
		ArticleListFragment fragment = new ArticleListFragment();
		return fragment;
	}

	public ArticleListFragment() {
		setHasOptionsMenu(true); // this enables us to set actionbar from
									// fragment
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		dba = new DbAdapter(getActivity());
		refreshList();
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		if (savedInstanceState != null
				&& savedInstanceState.containsKey(STATE_ACTIVATED_POSITION)) {
			setActivatedPosition(savedInstanceState
					.getInt(STATE_ACTIVATED_POSITION));
		}
	}

	@Override
	public void onResume() {
		ArticleListAdapter adapter = (ArticleListAdapter) this.getListAdapter();
		if (adapter != null) {
			refreshLocal();
		}

		super.onResume();

	}

	@Override
	public void onListItemClick(ListView listView, View view, int position,
			long id) {
		super.onListItemClick(listView, view, position, id);

		Article selected = (Article) this.getListAdapter().getItem((int) id);

		// mark article as read
		dba.openToWrite();
		dba.markAsRead(selected.getGuid());
		dba.close();
		selected.setRead(true);
		ArticleListAdapter adapter = (ArticleListAdapter) this.getListAdapter();
		adapter.notifyDataSetChanged();
		Log.e("CHANGE", "Changing to read: ");

		Intent detailIntent = new Intent(getActivity(),
				ArticleDetailActivity.class);
		detailIntent.putExtra(Article.KEY, selected);

		startActivity(detailIntent);
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if (mActivatedPosition != ListView.INVALID_POSITION) {
			outState.putInt(STATE_ACTIVATED_POSITION, mActivatedPosition);
		}
	}

	public void setActivateOnItemClick(boolean activateOnItemClick) {
		getListView().setChoiceMode(
				activateOnItemClick ? ListView.CHOICE_MODE_SINGLE
						: ListView.CHOICE_MODE_NONE);
	}

	public void setActivatedPosition(int position) {
		if (position == ListView.INVALID_POSITION) {
			getListView().setItemChecked(mActivatedPosition, false);
		} else {
			getListView().setItemChecked(position, true);
		}

		mActivatedPosition = position;
	}

	// @Override
	// public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
	// inflater.inflate(R.menu.refreshmenu, menu);
	// }

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.actionbar_refresh) {
			refreshLocal();
			return true;
		}
		return false;
	}

	private void refreshList() {
		rssService = new RssService(this);
		rssService.execute(BLOG_URL);
	}

	private void refreshLocal() {
		List<Article> list = new ArrayList<Article>();
		for (int i = 0; i < this.getListAdapter().getCount(); i++) {
			list.add((Article) this.getListAdapter().getItem(i));
		}
		rssService = new RssService(this);
		rssService.refreshLocal(list);
	}

}
