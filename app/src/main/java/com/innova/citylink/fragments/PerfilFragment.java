package com.innova.citylink.fragments;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.innova.citylink.R;
import com.innova.citylink.login.sessions.UserSessionManager;
import com.innova.citylink.util.StringUtils;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A simple {@link android.support.v4.app.Fragment} subclass. Activities that
 * contain this fragment must implement the
 * {@link HomeFragment.OnFragmentInteractionListener} interface to handle
 * interaction events. Use the {@link PerfilFragment#newInstance} factory method
 * to create an instance of this fragment.
 * 
 */
public class PerfilFragment extends Fragment implements
		SwipeRefreshLayout.OnRefreshListener {

	public static String URL_REPORT_ALL = "http://citylink.leadprogrammer.com.ar/cityReportService/rest/report/";
	private static final String CONTENT_TYPE = "Content-Type";
	private static final String CONTENT_TYPE_VALUE = "application/json";

	View rootView = null;
	UserSessionManager session;
	GetReportsTask getReportsTask = null;
	CustomSwipeRefreshLayout swipeLayout = null;
	// boolean swipeLayoutActive = false;

	private String mToken;

	private View mScrollViewPerfilView;
	private View mReports_statusView;
	private View swipeRefreshView;
	private TextView mReports_status_messageView;
	private ListView listView;
	private List<String> tempList = new ArrayList<String>();

	private List<Map<String, String>> listCategories = new ArrayList<Map<String, String>>();

	public static PerfilFragment newInstance(String mToken) {
		Bundle args = new Bundle();
		args.putString("token", mToken);

		PerfilFragment fragment = new PerfilFragment();
		fragment.setArguments(args);

		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		if (getArguments() != null) {
			mToken = getArguments().getString("token");
		}

		rootView = inflater.inflate(R.layout.fragment_perfil, container, false);

		session = new UserSessionManager(getActivity().getApplicationContext());

		TextView perfilView = (TextView) rootView.findViewById(R.id.tv_perfil);
		perfilView.setText(session.getUserDetails().get(
				UserSessionManager.KEY_EMAIL));

		listView = (ListView) rootView.findViewById(R.id.listReports);
		mScrollViewPerfilView = rootView.findViewById(R.id.scrollViewPerfil);
		mReports_statusView = rootView.findViewById(R.id.reports_status);
		swipeRefreshView = rootView.findViewById(R.id.swipeRefresh);

		swipeLayout = (CustomSwipeRefreshLayout) rootView
				.findViewById(R.id.swipe_container);
		swipeLayout.setOnRefreshListener(this);
		swipeLayout.setView((ListView) rootView.findViewById(R.id.listReports));
		// TODO: Fix me. Version Android

		swipeLayout.setColorScheme(getResources().getColor(android.R.color.holo_blue_bright),
				android.R.color.holo_green_light,
				android.R.color.holo_orange_light,
				android.R.color.holo_red_light);

		mReports_status_messageView = (TextView) rootView
				.findViewById(R.id.reports_status_message);

		loadGetReportsTask();
		checkShowActionBar();
		btnLogout();

		return rootView;
	}

	@Override
	public void onRefresh() {
		// swipeLayoutActive = true;
		loadGetReportsTask();
	}

	private void checkShowActionBar() {
		// CheckBox checkShowActionBar = (CheckBox) rootView
		// .findViewById(R.id.checkShowActionBar);
		// checkShowActionBar.setOnClickListener(new View.OnClickListener() {
		//
		// @SuppressLint("NewApi")
		// public void onClick(View v) {
		// if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
		// if (((CheckBox) v).isChecked()) {
		// getActivity().getActionBar().show();
		// } else {
		// getActivity().getActionBar().hide();
		// }
		// }
		// }
		// });
	}

	private void btnLogout() {
		Button btnLogout = (Button) rootView.findViewById(R.id.btnLogout);
		btnLogout.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {
				session.logoutUser();
			}
		});
	}

	private void loadGetReportsTask() {
		if (getReportsTask != null) {
			return;
		}

		// if (! swipeLayoutActive) {
		mReports_status_messageView.setText("Cargando reportes...");
		showProgress(true);
		// } else {
		//
		// }

		getReportsTask = new GetReportsTask();
		getReportsTask.execute((Void) null);
	}

	public class GetReportsTask extends AsyncTask<Void, Void, Boolean> {

		private static final String REPORTS = "reports";
		private String response = null;

		@Override
		protected Boolean doInBackground(Void... params) {
			HttpClient httpClient = new DefaultHttpClient();
			HttpGet httpGet = new HttpGet(URL_REPORT_ALL);
			httpGet.addHeader(CONTENT_TYPE, CONTENT_TYPE_VALUE
					+ ";charset=utf-8");
			httpGet.addHeader("Authorization", mToken);

			try {
				HttpResponse resp = httpClient.execute(httpGet);
				response = EntityUtils.toString(resp.getEntity(), HTTP.UTF_8);

				boolean haveReports = getArrayReportsFromJSON(response);

				return haveReports;
			} catch (Exception ex) {
				Log.e("GetReportsTask Exception", "Error!", ex);
				return false;
			}
		}

		@Override
		protected void onPostExecute(Boolean success) {
			getReportsTask = null;
			showProgress(false);
			if (swipeLayout.isRefreshing()) {
				swipeLayout.setRefreshing(false);
				// swipeLayoutActive = false;
			}

			if (success) {
				ArrayAdapter<String> adapter = new ArrayAdapter<String>(
						getActivity(), android.R.layout.simple_list_item_1,
						android.R.id.text1, tempList);
				listView.setAdapter(adapter);
				// ListView Item Click Listener
				listView.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> parent, View view,
							int position, long id) {

						// ListView Clicked item index
						int itemPosition = position;

						// ListView Clicked item value
						String itemValue = (String) listView
								.getItemAtPosition(position);

						// Show Alert
						Toast.makeText(
								getActivity().getApplicationContext(),
								"Position :" + itemPosition + "  ListItem : "
										+ itemValue, Toast.LENGTH_LONG).show();
					}

				});
			} else {

			}
		}

		@Override
		protected void onCancelled() {
			getReportsTask = null;
			showProgress(false);
		}

		private boolean getArrayReportsFromJSON(String jsonStr) {

			boolean parse = false;

			if (StringUtils.isNotEmpty(response)
					&& !response.startsWith("<html>")) {
				try {
					// temp:
					tempList = new ArrayList<String>();
					listCategories = new ArrayList<Map<String, String>>();

					JSONObject jsonObj = new JSONObject(response);
					JSONArray reports = jsonObj.getJSONArray(REPORTS);

					for (int i = 0; i < reports.length(); i++) {
						JSONObject jsonItem = reports.getJSONObject(i);
						JSONObject jsonObjectCategory = jsonItem
								.getJSONObject("category");
						JSONObject jsonObjectLocation = jsonItem
								.getJSONObject("location");

						String[] split = jsonObjectLocation
								.getString("address").split(",");

						HashMap<String, String> categoryItem = new HashMap<String, String>();
						categoryItem.put("description",
								jsonItem.getString("description"));
						categoryItem.put("imageUrl",
								jsonItem.getString("imageUrl"));
						categoryItem.put("name",
								jsonObjectCategory.getString("name"));
						categoryItem.put("address",
								jsonObjectLocation.getString("address"));

						tempList.add(jsonObjectCategory.getString("name")
								+ ", " + split[0]);
						listCategories.add(categoryItem);
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}

				if (listCategories.size() > 0) {
					parse = true;
				}
			}
			return parse;
		}
	}

	/**
	 * Shows the progress UI and hides the login form.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	private void showProgress(final boolean show) {

		// On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
		// for very easy animations. If available, use these APIs to fade-in
		// the progress spinner.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			if (!swipeLayout.isRefreshing()) {
				int shortAnimTime = getResources().getInteger(
						android.R.integer.config_shortAnimTime);

				mReports_statusView.setVisibility(View.VISIBLE);
				mReports_statusView.animate().setDuration(shortAnimTime)
						.alpha(show ? 1 : 0)
						.setListener(new AnimatorListenerAdapter() {
							@Override
							public void onAnimationEnd(Animator animation) {
								mReports_statusView
										.setVisibility(show ? View.VISIBLE
												: View.GONE);
							}
						});

				// mScrollViewPerfilView.setVisibility(View.VISIBLE);
				// mScrollViewPerfilView.animate().setDuration(shortAnimTime)
				// .alpha(show ? 0 : 1)
				// .setListener(new AnimatorListenerAdapter() {
				// @Override
				// public void onAnimationEnd(Animator animation) {
				// mScrollViewPerfilView
				// .setVisibility(show ? View.GONE
				// : View.VISIBLE);
				// }
				// });

			} else {
				int shortAnimTime = getResources().getInteger(
						android.R.integer.config_shortAnimTime);

				swipeRefreshView.setVisibility(View.VISIBLE);
				swipeRefreshView.animate().setDuration(shortAnimTime)
						.alpha(show ? 1 : 0)
						.setListener(new AnimatorListenerAdapter() {
							@Override
							public void onAnimationEnd(Animator animation) {
								swipeRefreshView
										.setVisibility(show ? View.VISIBLE
												: View.GONE);
							}
						});
			}

		} else {
			// The ViewPropertyAnimator APIs are not available, so simply show
			// and hide the relevant UI components.
			mReports_statusView.setVisibility(show ? View.VISIBLE : View.GONE);
			mScrollViewPerfilView
					.setVisibility(show ? View.GONE : View.VISIBLE);
		}
	}
}
