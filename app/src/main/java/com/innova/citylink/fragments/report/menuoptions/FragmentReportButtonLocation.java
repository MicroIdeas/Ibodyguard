package com.innova.citylink.fragments.report.menuoptions;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.android.gms.maps.model.LatLng;
import com.innova.citylink.R;
import com.innova.citylink.fragments.map.LocationMapFragment;
import com.innova.citylink.helpers.comunication.IActivityCommunicator;

public class FragmentReportButtonLocation extends Fragment {

	private Fragment fragmentTwo;
	private LatLng locationLatLng = null;
	private String streetLocation = null;
	private IActivityCommunicator activityCommunicator;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.fragment_report_button_location,
				container, false);
		Typeface tf = Typeface.createFromAsset(getActivity().getAssets(),
				"fonts/DistProTh.otf");

		Button buttonSayHi = (Button) view
				.findViewById(R.id.btn_report_location_op);
		buttonSayHi.setTypeface(tf);

		buttonSayHi.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				selectLocation(v);
			}
		});
		return view;
	}

	@Override
	public void onAttach(Activity activity) {
		activityCommunicator = (IActivityCommunicator) activity;
		super.onAttach(activity);
	}

	public void selectLocation(View view) {
		if (view == getView().findViewById(R.id.btn_report_location_op)) {
			// Launching new Activity on selecting single List Item
			Intent i = new Intent(getActivity(), LocationMapFragment.class);
			// sending data to new activity
			startActivityForResult(i, 1001);
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (resultCode == 1001) {
			streetLocation = data.getExtras().getString("LOC_STREET");
			String snapshotMap = data.getExtras().getString("snapshotMap");
			locationLatLng = (LatLng) data.getExtras().get("LOC_LATLNG");

			activityCommunicator.setStreetLocation(streetLocation);
			activityCommunicator.setLatLng(locationLatLng);

			fragmentTwo = new FragmentReportShowLocation();
			Bundle args = new Bundle();
			args.putString("resStreet", streetLocation);
			args.putString("snapshotMap", snapshotMap);
			fragmentTwo.setArguments(args);

			android.support.v4.app.FragmentManager fragmentManager = getFragmentManager();
			android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager
					.beginTransaction();

			fragmentTransaction.replace(R.id.fragment_report_location_option,
					fragmentTwo);
			fragmentTransaction.addToBackStack(null);
			fragmentTransaction.commit();
		} else {
			// TODO: implementar
		}
	}
}
