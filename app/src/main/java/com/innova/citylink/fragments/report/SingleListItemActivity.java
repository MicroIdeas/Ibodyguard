package com.innova.citylink.fragments.report;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.innova.citylink.MainActivity;
import com.innova.citylink.R;
import com.innova.citylink.fragments.CategoryListReportFragment;
import com.innova.citylink.helpers.comunication.IActivityCommunicator;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;

import java.io.IOException;

public class SingleListItemActivity extends FragmentActivity implements
		IActivityCommunicator {

	private String URL_REPORT = "http://citylink.leadprogrammer.com.ar/cityReportService/rest/report";
	private String URL_IMAGE_UPLOAD = "http://citylink.leadprogrammer.com.ar/cityReportService/rest/image/upload";

	private static final String CONTENT_TYPE = "Content-Type";
	private static final String CONTENT_TYPE_VALUE = "application/json";

	private UserLoginTask mAuthTask = null;

	private static final String CATEGORY_NAME = "categoryName";
	private static final String CATEGORY_ID = "id";
	private String categoryName = null;
	private String categoryId = null;
	private LatLng locationLatLng = null;
	private String streetLocation = null;

	private EditText descriptionEditText = null;

	// private static final int PICK_IMAGE = 1;
	// private static final int PICK_Camera_IMAGE = 2;
	Button btnSelectPhoto;
	Uri imageUri;
	ByteArrayBody bab;
	private String mToken;

	private View mLoginFormView;
	private View mLoginStatusView;
	private TextView mLoginStatusMessageView;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.activity_single_list_item);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}

		Typeface tf = Typeface.createFromAsset(getAssets(),
				"fonts/DistProTh.otf");
		TextView txtDescription = (TextView) findViewById(R.id.report_label_description);
		txtDescription.setTypeface(tf);

		descriptionEditText = (EditText) findViewById(R.id.report_txt_description);
		// hide keyboard
		// InputMethodManager imm = (InputMethodManager)getSystemService(
		// Context.INPUT_METHOD_SERVICE);
		// imm.hideSoftInputFromWindow(descriptionView.getWindowToken(), 0);
		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		// hideKeyboard();

		TextView txtIssue = (TextView) findViewById(R.id.issue_label);

		Intent i = getIntent();
		// getting attached intent data
		categoryName = i.getStringExtra(CATEGORY_NAME);
		categoryId = i.getStringExtra(CATEGORY_ID);
		mToken = i.getStringExtra("token");
		// displaying selected issue name
		txtIssue.setText(getString(R.string.txt_reason) + " " + categoryName);

		mLoginFormView = findViewById(R.id.scrollViewReport);
		mLoginStatusView = findViewById(R.id.login_status);
		mLoginStatusMessageView = (TextView) findViewById(R.id.login_status_message);
	}

	public void btnCancel(View view) {
		if (view == findViewById(R.id.btnCancel)) {
			NavUtils.navigateUpTo(this, new Intent(this,
					CategoryListReportFragment.class));
		}
	}

	public void btnAccept(View view) {
		if (view == findViewById(R.id.btnAccept)) {
			if (validationData()) {
				mLoginStatusMessageView.setText(R.string.report_progress_send);
				showProgress(true);

				mAuthTask = new UserLoginTask();
				mAuthTask.execute((Void) null);
			}
		}
	}

	private boolean validationData() {
		boolean isOk = true;
		
		if (locationLatLng == null || streetLocation == null || streetLocation.equals("")) {
			openMessageDialog("Faltan datos", "Debe selecionar una ubicacion");
			isOk = false;
		} else if (bab == null) {
			openMessageDialog("Faltan datos", "Debe cargar una foto");
			isOk = false;
		} else if (TextUtils.isEmpty(descriptionEditText.getText().toString())) {
			descriptionEditText.setError("Debe ingresar una descripcion");
			isOk = false;
		}
		
		return isOk;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			NavUtils.navigateUpTo(this, new Intent(this,
					CategoryListReportFragment.class));
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	/**
	 * Shows the progress UI and hides the login form.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	private void showProgress(final boolean show) {
		// On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
		// for very easy animations. If available, use these APIs to fade-in
		// the progress spinner.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			int shortAnimTime = getResources().getInteger(
					android.R.integer.config_shortAnimTime);

			mLoginStatusView.setVisibility(View.VISIBLE);
			mLoginStatusView.animate().setDuration(shortAnimTime)
					.alpha(show ? 1 : 0)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							mLoginStatusView.setVisibility(show ? View.VISIBLE
									: View.GONE);
						}
					});

//			mLoginFormView.setVisibility(View.VISIBLE);
//			mLoginFormView.animate().setDuration(shortAnimTime)
//					.alpha(show ? 0 : 1)
//					.setListener(new AnimatorListenerAdapter() {
//						@Override
//						public void onAnimationEnd(Animator animation) {
//							mLoginFormView.setVisibility(show ? View.GONE
//									: View.VISIBLE);
//						}
//					});
		} else {
			// The ViewPropertyAnimator APIs are not available, so simply show
			// and hide the relevant UI components.
			mLoginStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
			mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
		}
	}

	/**
	 * Represents an asynchronous task
	 */
	public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {
		private String error = "error";

		@Override
		protected Boolean doInBackground(Void... params) {
			boolean resul = true;

			HttpClient httpClient = new DefaultHttpClient();
			HttpPost postImage = new HttpPost(URL_IMAGE_UPLOAD);
			HttpPost postReport = new HttpPost(URL_REPORT);

			MultipartEntityBuilder builder = MultipartEntityBuilder.create();
			builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
			builder.addPart("file", bab);

			HttpEntity entityImg = builder.build();
			postImage.setEntity(entityImg);

			try {
				HttpResponse image_response = httpClient.execute(postImage);

				// check 200 OK for image_response success
				int statusCode = image_response.getStatusLine().getStatusCode();

				if (statusCode == HttpStatus.SC_OK) {
					String respStrImg = EntityUtils.toString(image_response
							.getEntity());
					JSONObject jsonObjectImg = getJSONObject(respStrImg);

					if (jsonObjectImg != null) {
						String img_id = jsonObjectImg.get("id").toString();

						if (img_id != null && !img_id.equals("")) {
							// rest report
							postReport.addHeader(CONTENT_TYPE,
									CONTENT_TYPE_VALUE + ";charset=utf-8");
							postReport.addHeader("Authorization", mToken);

							JSONObject category = new JSONObject();
							category.put("id", categoryId);
							category.put("name", categoryName);

							JSONObject location = new JSONObject();
							location.put("latitude", locationLatLng.latitude);
							location.put("longitude", locationLatLng.longitude);
							location.put("address", streetLocation);

							JSONObject report = new JSONObject();
							report.put("description",
									descriptionEditText.getText());
							report.put("category", category);
							report.put("location", location);
							report.put("imageUrl", img_id);

							StringEntity entity = new StringEntity(
									report.toString(), HTTP.UTF_8);
							postReport.setEntity(entity);

							HttpResponse report_response = httpClient
									.execute(postReport);
//							String respStr = EntityUtils
//									.toString(report_response.getEntity());

							// //check 200 OK for success
							statusCode = report_response.getStatusLine()
									.getStatusCode();

							if (statusCode != HttpStatus.SC_OK) {
								// fail report send statusCode
								error = "Error enviando reporte. Code: "
										+ statusCode;
								resul = false;
							}
						} else {
							// no image id
							error = "No hay ID de imagen";
							resul = false;
						}
					} else {
						// response vacio
						error = "Response imagen de reporte vacio";
						resul = false;
					}
				} else {
					// fail image upload statusCode
					// add un reintentar
					error = "Error enviando imagen del reporte. Code: " + statusCode;
					resul = false;
				}
			} catch (ClientProtocolException e) {
				resul = false;
				error = "ClientProtocolException";
				e.printStackTrace();
			} catch (IOException e) {
				resul = false;
				error = "IOException";
				e.printStackTrace();
			} catch (JSONException e) {
				resul = false;
				error = "JSONException";
				e.printStackTrace();
			}

			return resul;
		}

		private JSONObject getJSONObject(String jsonStr) throws JSONException {
			JSONObject jsonObj = new JSONObject(jsonStr);

			return jsonObj;
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			mAuthTask = null;
			showProgress(false);

			if (success) {
				openSuccessDialog();
			} else {
				System.out.println(error);
				openMessageDialog("Error enviado reporte :(", "Ups! no se pudo enviar el reporte. " + error);
			}
		}

		@Override
		protected void onCancelled() {
			mAuthTask = null;
			showProgress(false);
		}
	}

	private void openSuccessDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(
				"Gracias por comunicarse. El reporte fue enviado exitosamente.")
				.setTitle("Reporte enviado")
				.setCancelable(false)
				.setNeutralButton("Aceptar",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// dialog.cancel();
								Intent i = new Intent(getApplicationContext(),
										MainActivity.class);
								startActivity(i);
							}
						});
		AlertDialog alert = builder.create();
		alert.show();
	}

	private void openFailDialog(String title, String message) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(
				message)
				.setTitle(title)
				.setCancelable(false)
				.setNeutralButton("Aceptar",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// dialog.cancel();
								Intent i = new Intent(getApplicationContext(),
										MainActivity.class);
								startActivity(i);
							}
						});
		AlertDialog alert = builder.create();
		alert.show();
	}
	
	private void openMessageDialog(String title, String message) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(
				message)
				.setTitle(title)
				.setCancelable(false)
				.setNeutralButton("Aceptar",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								 dialog.cancel();
							}
						});
		AlertDialog alert = builder.create();
		alert.show();
	}

	// private void hideKeyboard() {
	// // Lineas para ocultar el teclado virtual (Hide keyboard)
	// InputMethodManager imm = (InputMethodManager)
	// getSystemService(Context.INPUT_METHOD_SERVICE);
	// imm.hideSoftInputFromWindow(descriptionEditText.getWindowToken(), 0);
	// }

	@Override
	public void setStreetLocation(String streetLocationValue) {
		streetLocation = streetLocationValue;
	}

	@Override
	public void setLatLng(LatLng latLngValue) {
		locationLatLng = latLngValue;
	}

	@Override
	public void setPhotoReport(ByteArrayBody byteArrayBodyValue) {
		bab = byteArrayBodyValue;
	}
}
