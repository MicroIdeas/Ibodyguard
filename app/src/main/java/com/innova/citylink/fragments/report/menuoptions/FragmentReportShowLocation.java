package com.innova.citylink.fragments.report.menuoptions;

import android.annotation.SuppressLint;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.innova.citylink.R;

public class FragmentReportShowLocation extends Fragment {

	View view;
	Fragment fragmentOne;
	private TextView txtLocation = null;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Typeface tf = Typeface.createFromAsset(getActivity().getAssets(),
				"fonts/DistProTh.otf");
		String street = getArguments().getString("resStreet");
		String snapshotMap = getArguments().getString("snapshotMap");

		View view = inflater.inflate(R.layout.fragment_report_show_location, container, false);
		txtLocation = (TextView) view.findViewById(R.id.txt_location);
		txtLocation.setTypeface(tf);
		txtLocation.setText(street);

		Button buttonSayHi = (Button) view
				.findViewById(R.id.btn_report_location_change_op);
		buttonSayHi.setTypeface(tf);

		Drawable background = Drawable.createFromPath(snapshotMap);
		setBackgroundDrawable(view, background);

		buttonSayHi.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				fragmentOne = new FragmentReportButtonLocation();
				android.support.v4.app.FragmentManager fragmentManager = getFragmentManager();
				android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager
						.beginTransaction();
				
				fragmentTransaction.replace(
						R.id.fragment_report_location_option, fragmentOne);
				fragmentTransaction.addToBackStack(null);
				fragmentTransaction.commit();
			}
		});
		return view;
	}

	@SuppressLint("NewApi")
	@SuppressWarnings("deprecation")
	public static void setBackgroundDrawable(View view, Drawable drawable) {
		if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
			view.setBackgroundDrawable(drawable);
		} else {
			view.setBackground(drawable);
		}
	}
}
