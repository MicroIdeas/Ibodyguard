package com.innova.citylink.fragments.report;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.innova.citylink.R;
import com.innova.citylink.adapter.ListViewArrayAdapter;

/**
 * A simple {@link android.support.v4.app.Fragment} subclass. Activities that
 * contain this fragment must implement the
 * {@link ReportsFragment.OnFragmentInteractionListener} interface to handle
 * interaction events. Use the {@link ReportsFragment#newInstance} factory
 * method to create an instance of this fragment.
 * 
 */
public class ReportsFragment extends ListFragment {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// storing string resources into Array
		String[] issues_list = getResources().getStringArray(
				R.array.issues_list);

		// Binding resources Array to ListAdapter
		this.setListAdapter(new ListViewArrayAdapter(this.getActivity(),
				R.layout.list_item, R.id.label, issues_list));
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);

		// selected item
		String issue = ((TextView) v).getText().toString();

		// Launching new Activity on selecting single List Item
		Intent i = new Intent(getActivity(), SingleListItemActivity.class);
		// sending data to new activity
		i.putExtra("issue", issue);
		startActivity(i);
	}

}
