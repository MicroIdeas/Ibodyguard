package com.innova.citylink.fragments.report.menuoptions;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.innova.citylink.R;

/**
 * A simple {@link android.support.v4.app.Fragment} subclass. Activities that
 * contain this fragment must implement the
 * {@link FragmentReportShowPhoto.OnFragmentInteractionListener} interface to handle
 * interaction events. Use the {@link FragmentReportShowPhoto#newInstance} factory method
 * to create an instance of this fragment.
 * 
 */
public class FragmentReportShowPhoto extends Fragment {

	// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
//	private static final String ARG_PARAM1 = "param1";
//	private static final String ARG_PARAM2 = "param2";


//	private String mParam1;
//	private String mParam2;

	View view;
	Fragment fragmentThree;

	ImageView viewImage;

//	/**
//	 * Use this factory method to create a new instance of this fragment using
//	 * the provided parameters.
//	 * 
//	 * @param param1
//	 *            Parameter 1.
//	 * @param param2
//	 *            Parameter 2.
//	 * @return A new instance of fragment FourFragment.
//	 */
//	public static FragmentFour newInstance(String param1, String param2) {
//		FragmentFour fragment = new FragmentFour();
//		Bundle args = new Bundle();
//		args.putString(ARG_PARAM1, param1);
//		args.putString(ARG_PARAM2, param2);
//		fragment.setArguments(args);
//		return fragment;
//	}
//
//	public FragmentFour() {
//		// Required empty public constructor
//	}
//
//	@Override
//	public void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		if (getArguments() != null) {
//			mParam1 = getArguments().getString(ARG_PARAM1);
//			mParam2 = getArguments().getString(ARG_PARAM2);
//		}
//	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View view = inflater.inflate(R.layout.fragment_report_show_photo, container, false);
		viewImage = (ImageView) view.findViewById(R.id.viewImage);
		Bitmap bitmap = getArguments().getParcelable("reportPhoto");
		viewImage.setImageBitmap(bitmap);

		Button buttonSayHi = (Button) view
				.findViewById(R.id.btn_report_photo_change_op);
		buttonSayHi.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				fragmentThree = new FragmentReportButtonPhoto();

				android.support.v4.app.FragmentManager fragmentManager = getFragmentManager();
				android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager
						.beginTransaction();
				
				fragmentTransaction.replace(R.id.fragment_report_photo_option,
						fragmentThree);
				fragmentTransaction.addToBackStack(null);
				fragmentTransaction.commit();
			}
		});
		return view;
	}

}
