package com.innova.citylink.fragments;

import android.content.res.AssetManager;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.List;

public class ServiceHandler {

	static String response = null;
	public final static int GET = 1;
	public final static int POST = 2;
	private AssetManager assetManager;

	public ServiceHandler() {

	}

	/*
	 * Making service call
	 * 
	 * @url - url to make request
	 * 
	 * @method - http request method
	 */
	public String makeServiceCall(String url, int method) {
		return this.makeServiceCall(url, method, null);
	}

	/*
	 * Making service call
	 * 
	 * @url - url to make request
	 * 
	 * @method - http request method
	 * 
	 * @params - http request params
	 */
	public String makeServiceCall(String url, int method,
			List<NameValuePair> params) {
		try {
			// http client
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpEntity httpEntity = null;
			HttpResponse httpResponse = null;

			// Checking http request method type
			if (method == POST) {
				HttpPost httpPost = new HttpPost(url);
				// adding post params
				if (params != null) {
					httpPost.setEntity(new UrlEncodedFormEntity(params));
				}

				httpResponse = httpClient.execute(httpPost);

			} else if (method == GET) {
				// appending params to url
				if (params != null) {
					String paramString = URLEncodedUtils
							.format(params, "utf-8");
					url += "?" + paramString;
				}
				HttpGet httpGet = new HttpGet(url);

				httpResponse = httpClient.execute(httpGet);

			}
			httpEntity = httpResponse.getEntity();
			response = EntityUtils.toString(httpEntity);

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return response;

	}

	// ///////////////// MOCK
	public String makeServiceCallMock(String url, int method) {
		return this.makeServiceCallMock(url, method, null);
	}

	public String makeServiceCallMock(String url, int method,
			AssetManager assetManager) {
		this.assetManager = assetManager;
		
//		response = "[{\"id\": \"1\",\"name\":\"Bache\",\"description\":\"Baches en calles\",\"father\":null,\"priority\":\"low\",\"categorygroup\":{\"id\": \"2\",\"name\": \"Calles\",\"description\": \"Problemas en las calles/calzadas\"}},{\"id\": \"2\",\"name\":\"Falta de Iluminacion\",\"description\":\"Falta de Iluminacion\",\"father\":null,\"priority\":\"low\",\"categorygroup\":{\"id\": \"2\",\"name\": \"Calles\",\"description\": \"Problemas en las calles/calzadas\"}},{\"id\": \"3\",\"name\":\"Vandalismo\",\"description\":\"Vandalismo\",\"father\":null,\"priority\":\"low\",\"categorygroup\":{\"id\": \"2\",\"name\": \"Calles\",\"description\": \"Problemas en las calles/calzadas\"}}]";
//		response = "{\"contacts\":[{\"id\":\"c200\",\"name\":\"RaviTamada\",\"email\":\"ravi@gmail.com\",\"address\":\"xx-xx-xxxx,x-street,x-country\",\"gender\":\"male\",\"phone\":{\"mobile\":\"+910000000000\",\"home\":\"00000000\",\"office\":\"00000000\"}},{\"id\":\"c201\",\"name\":\"JohnnyDepp\",\"email\":\"johnny_depp@gmail.com\",\"address\":\"xx-xx-xxxx,x-street,x-country\",\"gender\":\"male\",\"phone\":{\"mobile\":\"+910000000000\",\"home\":\"00000000\",\"office\":\"00000000\"}},{\"id\":\"c202\",\"name\":\"LeonardoDicaprio\",\"email\":\"leonardo_dicaprio@gmail.com\",\"address\":\"xx-xx-xxxx,x-street,x-country\",\"gender\":\"male\",\"phone\":{\"mobile\":\"+910000000000\",\"home\":\"00000000\",\"office\":\"00000000\"}},{\"id\":\"c203\",\"name\":\"JohnWayne\",\"email\":\"john_wayne@gmail.com\",\"address\":\"xx-xx-xxxx,x-street,x-country\",\"gender\":\"male\",\"phone\":{\"mobile\":\"+910000000000\",\"home\":\"00000000\",\"office\":\"00000000\"}},{\"id\":\"c204\",\"name\":\"AngelinaJolie\",\"email\":\"angelina_jolie@gmail.com\",\"address\":\"xx-xx-xxxx,x-street,x-country\",\"gender\":\"female\",\"phone\":{\"mobile\":\"+910000000000\",\"home\":\"00000000\",\"office\":\"00000000\"}},{\"id\":\"c205\",\"name\":\"Dido\",\"email\":\"dido@gmail.com\",\"address\":\"xx-xx-xxxx,x-street,x-country\",\"gender\":\"female\",\"phone\":{\"mobile\":\"+910000000000\",\"home\":\"00000000\",\"office\":\"00000000\"}},{\"id\":\"c206\",\"name\":\"Adele\",\"email\":\"adele@gmail.com\",\"address\":\"xx-xx-xxxx,x-street,x-country\",\"gender\":\"female\",\"phone\":{\"mobile\":\"+910000000000\",\"home\":\"00000000\",\"office\":\"00000000\"}},{\"id\":\"c207\",\"name\":\"HughJackman\",\"email\":\"hugh_jackman@gmail.com\",\"address\":\"xx-xx-xxxx,x-street,x-country\",\"gender\":\"male\",\"phone\":{\"mobile\":\"+910000000000\",\"home\":\"00000000\",\"office\":\"00000000\"}},{\"id\":\"c208\",\"name\":\"WillSmith\",\"email\":\"will_smith@gmail.com\",\"address\":\"xx-xx-xxxx,x-street,x-country\",\"gender\":\"male\",\"phone\":{\"mobile\":\"+910000000000\",\"home\":\"00000000\",\"office\":\"00000000\"}},{\"id\":\"c209\",\"name\":\"ClintEastwood\",\"email\":\"clint_eastwood@gmail.com\",\"address\":\"xx-xx-xxxx,x-street,x-country\",\"gender\":\"male\",\"phone\":{\"mobile\":\"+910000000000\",\"home\":\"00000000\",\"office\":\"00000000\"}},{\"id\":\"c2010\",\"name\":\"BarackObama\",\"email\":\"barack_obama@gmail.com\",\"address\":\"xx-xx-xxxx,x-street,x-country\",\"gender\":\"male\",\"phone\":{\"mobile\":\"+910000000000\",\"home\":\"00000000\",\"office\":\"00000000\"}},{\"id\":\"c2011\",\"name\":\"KateWinslet\",\"email\":\"kate_winslet@gmail.com\",\"address\":\"xx-xx-xxxx,x-street,x-country\",\"gender\":\"female\",\"phone\":{\"mobile\":\"+910000000000\",\"home\":\"00000000\",\"office\":\"00000000\"}},{\"id\":\"c2012\",\"name\":\"Eminem\",\"email\":\"eminem@gmail.com\",\"address\":\"xx-xx-xxxx,x-street,x-country\",\"gender\":\"male\",\"phone\":{\"mobile\":\"+910000000000\",\"home\":\"00000000\",\"office\":\"00000000\"}}]}";
		
		
		try {
			response = getStringFromAssetFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return response;

	}
	
	private String getStringFromAssetFile() throws IOException
	{
//		AssetManager am = activity.getAssets();
		InputStream is = assetManager.open("json.txt");
		String s = getStringFromInputStreamMock(is);
		is.close();
		return s;
	}
	
	// convert InputStream to String
	private static String getStringFromInputStreamMock(InputStream is) {
 
		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();
 
		String line;
		try {
 
			br = new BufferedReader(new InputStreamReader(is, "ISO-8859-1"));
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}
 
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
 
		return sb.toString();
 
	}
	
}
