package com.innova.citylink.fragments.report.menuoptions;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.innova.citylink.R;
import com.innova.citylink.helpers.comunication.IActivityCommunicator;
import com.innova.citylink.photo.PhotoUtil;

import org.apache.http.entity.mime.content.ByteArrayBody;

import java.io.ByteArrayOutputStream;
import java.io.File;

/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 * 
 */
public class FragmentReportButtonPhoto extends Fragment {

	View view;
	Fragment fragmentFour;

	Uri imageUri;
	private Bitmap bitmap;
	ByteArrayBody bab;
	private IActivityCommunicator activityCommunicator;

	public FragmentReportButtonPhoto() {
		// Required empty public constructor
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout for this fragment

		View view = inflater.inflate(R.layout.fragment_report_button_photo,
				container, false);
		Typeface tf = Typeface.createFromAsset(getActivity().getAssets(),
				"fonts/DistProTh.otf");

		Button buttonSayHi = (Button) view
				.findViewById(R.id.btn_report_photo_op);
		buttonSayHi.setTypeface(tf);

		buttonSayHi.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				selectImage();
			}
		});
		return view;
	}

	@Override
	public void onAttach(Activity activity) {
		activityCommunicator = (IActivityCommunicator) activity;
		super.onAttach(activity);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == 1) {

			String filePath = null;

			try {

				// OI FILE Manager
				String filemanagerstring = imageUri.getPath();

				// // MEDIA GALLERY
				// String selectedImagePath = getPath(imageUri);

				// if (selectedImagePath != null) {
				// filePath = selectedImagePath;
				// } else
				if (filemanagerstring != null) {
					filePath = filemanagerstring;
				} else {
					// Toast.makeText(getApplicationContext(), "Unknown path",
					// Toast.LENGTH_LONG).show();
					Toast.makeText(getActivity(), "Unknown path",
							Toast.LENGTH_LONG).show();
					Log.e("Bitmap", "Unknown path");
				}

				if (filePath != null) {
					decodeFile(filePath);
				} else {
					bitmap = null;
				}
			} catch (Exception e) {
				// Toast.makeText(getApplicationContext(), "Internal error",
				// Toast.LENGTH_LONG).show();
				Toast.makeText(getActivity(), "Internal error",
						Toast.LENGTH_LONG).show();
				Log.e(e.getClass().getName(), e.getMessage(), e);
			}

		} else if (requestCode == 2) {
			// Uri selectedImageUri = data.getData();
			// String p=selectedImageUri.getPath();
			//
			// String tempPath = data.getDataString();
			// Log.i("path of image from gallery......******************.........",
			// p);
			// Bitmap bitmap;
			// BitmapFactory.Options bitmapOptions = new
			// BitmapFactory.Options();
			// bitmap = BitmapFactory.decodeFile(tempPath, bitmapOptions);
			// viewImage.setImageBitmap(bitmap);

			Uri selectedImage = data.getData();
			String[] filePath = { MediaStore.Images.Media.DATA };
			Cursor c = getActivity().getContentResolver().query(selectedImage,
					filePath, null, null, null);
			c.moveToFirst();
			int columnIndex = c.getColumnIndex(filePath[0]);
			String picturePath = c.getString(columnIndex);
			c.close();
			// Bitmap thumbnail = (BitmapFactory.decodeFile(picturePath));
			// Log.w("path of image from gallery......******************.........",
			// picturePath+"");
			// viewImage.setImageBitmap(thumbnail);

			decodeFile(picturePath);
		}

		if (bitmap != null) {
			activityCommunicator.setPhotoReport(bab);

			fragmentFour = new FragmentReportShowPhoto();
			Bundle args = new Bundle();
			args.putParcelable("reportPhoto", bitmap);
			fragmentFour.setArguments(args);

			android.support.v4.app.FragmentManager fragmentManager = getFragmentManager();
			android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager
					.beginTransaction();

			// FragmentManager fragmentManager = getFragmentManager();
			// FragmentTransaction fragmentTransaction =
			// fragmentManager.beginTransaction();
			fragmentTransaction.replace(R.id.fragment_report_photo_option,
					fragmentFour);
			fragmentTransaction.addToBackStack(null);

			fragmentTransaction.commit();
		} else {
			// TODO: algo salio mal?
		}
	}

	// Selecting image from Gallery or taking image from camera
	private void selectImage() {
		final CharSequence[] options = { "Tomar Foto", "Elegir de la Galeria",
				"Cancelar" };

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Agregar Foto");
		builder.setItems(options, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int item) {
				if (options[item].equals("Tomar Foto")) {
					// Intent intent = new
					// Intent(MediaStore.ACTION_IMAGE_CAPTURE);
					// File f = new
					// File(android.os.Environment.getExternalStorageDirectory(),
					// "temp.jpg");
					// intent.putExtra(MediaStore.EXTRA_OUTPUT,
					// Uri.fromFile(f));
					// startActivityForResult(intent, 1);

					// // File file = PhotoUtil.getFile(getActivity());
					// Intent intent = new
					// Intent(MediaStore.ACTION_IMAGE_CAPTURE);
					// // intent.putExtra("LEO",
					// // Uri.fromFile(file));
					// startActivityForResult(intent, 1);

					Intent takePictureIntent = new Intent(
							MediaStore.ACTION_IMAGE_CAPTURE);
					// Ensure that there's a camera activity to handle the
					// intent
					if (takePictureIntent.resolveActivity(getActivity()
							.getPackageManager()) != null) {
						// Create the File where the photo should go
						File photoFile = null;

						photoFile = PhotoUtil.getFile(getActivity());

						// Continue only if the File was successfully created
						if (photoFile != null) {
							takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
									Uri.fromFile(photoFile));
							imageUri = Uri.fromFile(photoFile);
							startActivityForResult(takePictureIntent, 1);
						}
					}

					// // create parameters for Intent with filename
					// ContentValues values = new ContentValues();
					// values.put(MediaStore.Images.Media.TITLE, "temp.jpg");
					// //
					// values.put(MediaStore.Images.Media.DESCRIPTION,"Image captured by camera");
					// // imageUri is the current activity attribute, define and
					// // save it for later usage (also in onSaveInstanceState)
					// imageUri = getActivity().getContentResolver().insert(
					// MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
					// values);
					// // create new Intent
					// Intent intent = new
					// Intent(MediaStore.ACTION_IMAGE_CAPTURE);
					// intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
					// startActivityForResult(intent, 1);
				} else if (options[item].equals("Elegir de la Galeria")) {
					// Intent intent = new
					// Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
					// intent.setType("image/*");
					// startActivityForResult(Intent.createChooser(intent,
					// "Select File"),2);

					Intent intent = new Intent(
							Intent.ACTION_PICK,
							android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
					startActivityForResult(intent, 2);
					//
					// Intent photoPickerIntent = new
					// Intent(Intent.ACTION_PICK);
					// photoPickerIntent.setType("image/*");
					// startActivityForResult(photoPickerIntent, 2);

				} else if (options[item].equals("Cancelar")) {
					dialog.dismiss();
				}
			}
		});
		builder.show();
	}

	public String getPath(Uri uri) {
		String[] filePath = { MediaStore.Images.Media.DATA };
		Cursor c = getActivity().getContentResolver().query(uri, filePath,
				null, null, null);
		c.moveToFirst();
		int columnIndex = c.getColumnIndex(filePath[0]);
		String picturePath = c.getString(columnIndex);
		c.close();
		return picturePath;

		// String[] projection = { MediaStore.Images.Media.DATA };
		// Cursor cursor = managedQuery(uri, projection, null, null, null);
		// if (cursor != null) {
		// // HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
		// // THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
		// int column_index = cursor
		// .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		// cursor.moveToFirst();
		// return cursor.getString(column_index);
		// } else
		// return null;
	}

	public void decodeFile(String filePath) {
		// Decode image size
		BitmapFactory.Options o = new BitmapFactory.Options();
		o.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(filePath, o);

		// The new size we want to scale to
		final int REQUIRED_SIZE = 1524;

		// Find the correct scale value. It should be the power of 2.
		int width_tmp = o.outWidth, height_tmp = o.outHeight;
		int scale = 1;
		while (true) {
			if (width_tmp < REQUIRED_SIZE && height_tmp < REQUIRED_SIZE)
				break;
			width_tmp /= 2;
			height_tmp /= 2;
			scale *= 2;
		}

		// Decode with inSampleSize
		BitmapFactory.Options o2 = new BitmapFactory.Options();
		o2.inSampleSize = scale;
		bitmap = BitmapFactory.decodeFile(filePath, o2);

		// viewImage.setImageBitmap(bitmap);

		// Para enviar por RETS
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		bitmap.compress(CompressFormat.JPEG, 100, bos);
		byte[] bitmapdata = bos.toByteArray();
		bab = new ByteArrayBody(bitmapdata, "tmp_rest.jpg");
	}
}
