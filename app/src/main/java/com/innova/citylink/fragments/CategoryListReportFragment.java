package com.innova.citylink.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import com.innova.citylink.R;
import com.innova.citylink.fragments.report.SingleListItemActivity;
import com.innova.citylink.headerlistview.HeaderListView;
import com.innova.citylink.headerlistview.SectionAdapter;
import com.innova.citylink.login.LoginConstant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CategoryListReportFragment extends Fragment {

	private static final String CATEGORY_NAME = "categoryName";

	// JSON Node names
	private static final String TAG_CATEGORIES = "categories";

	private static final String TAG_ID = "id";
	private static final String TAG_NAME = "name";
	private static final String TAG_DESCRIPTION = "description";
	private static final String TAG_CATEGORY_GROUP = "categorygroup";
	private static final String TAG_CATEGORY_GROUP_NAME = "name";

	JSONArray categories = null;
	HashMap<String, List> categoryGroupList = new HashMap<String, List>();
	HashMap<String, String> categoryGroupNames = new HashMap<String, String>();
	
	private String mToken;
	
    public static CategoryListReportFragment newInstance(String mToken) {
		Bundle args = new Bundle();
	    args.putString("token", mToken);
    	
    	CategoryListReportFragment fragment = new CategoryListReportFragment();
    	fragment.setArguments(args);
    	
        return fragment;
    }	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		
		mToken = getArguments().getString("token");
//		mToken = getActivity().getIntent().getStringExtra("token");
		
		// Creating service handler class instance
		ServiceHandler sh = new ServiceHandler();

		// Making a request to url and getting response
//		String jsonStr = sh.makeServiceCallMock("url", ServiceHandler.GET,
//				getActivity().getAssets());
		
		String jsonStr = "";
		try {
			FileInputStream is = getActivity().openFileInput(LoginConstant.CATEGORIES_JSON);
			jsonStr = getStringFromInputStreamMock(is);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		getArrayCategoriesAdapterFormatJSON(jsonStr);
	}
	
	//move to ultis or stringsutils
	// convert InputStream to String
	private static String getStringFromInputStreamMock(InputStream is) {
 
		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();
 
		String line;
		try {
			br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}
 
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
 
		return sb.toString();
 
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);

		HeaderListView list = new HeaderListView(getActivity());

		list.setAdapter(new SectionAdapter() {

			@Override
			public int numberOfSections() {
				return categoryGroupList.size();
			}

			@Override
			public int numberOfRows(int section) {

				List sectionList = categoryGroupList.get(String
						.valueOf(section));
				// TODO controlar el null
				return sectionList.size();
			}

			@Override
			public Object getRowItem(int section, int row) {
				return null;
			}

			@Override
			public boolean hasSectionHeaderView(int section) {
				return true;
			}

			@Override
			public View getRowView(int section, int row, View convertView,
					ViewGroup parent) {
				if (convertView == null) {
					convertView = getActivity().getLayoutInflater().inflate(
							getResources().getLayout(
									R.layout.simple_list_item_2), null);
				}

				// TODO: pasar esto a un metodo unico
				List<HashMap<String, String>> sectionList = categoryGroupList
						.get(String.valueOf(section));

				((TextView) convertView.findViewById(R.id.text1))
						.setText(sectionList.get(row).get(TAG_NAME));
				((TextView) convertView.findViewById(R.id.text2))
						.setText(sectionList.get(row).get(TAG_DESCRIPTION));

				return convertView;
			}

			@Override
			public int getSectionHeaderViewTypeCount() {
				return 2;
			}

			@Override
			public int getSectionHeaderItemViewType(int section) {
				return super.getSectionHeaderItemViewType(section);
			}

			@Override
			public View getSectionHeaderView(int section, View convertView,
					ViewGroup parent) {

				if (convertView == null) {
					convertView = getActivity().getLayoutInflater().inflate(
							getResources().getLayout(
									R.layout.simple_list_item_1), null);
				}

				((TextView) convertView.findViewById(R.id.text1))
						.setText(categoryGroupNames.get(String
										.valueOf(section)));

				switch (section) {
				case 0:
					convertView.setBackgroundColor(getResources().getColor(
							R.color.holo_red_light));
					break;
				case 1:
					convertView.setBackgroundColor(getResources().getColor(
							R.color.holo_orange_light));
					break;
				case 2:
					convertView.setBackgroundColor(getResources().getColor(
							R.color.holo_green_light));
					break;
				case 3:
					convertView.setBackgroundColor(getResources().getColor(
							R.color.holo_blue_light));
					break;
				}
				return convertView;
			}

			@Override
			public void onRowItemClick(AdapterView<?> parent, View view,
					int section, int row, long id) {
				super.onRowItemClick(parent, view, section, row, id);

				List<HashMap<String, String>> sectionList = categoryGroupList
						.get(String.valueOf(section));

				Intent i = new Intent(getActivity(),
						SingleListItemActivity.class);
				i.putExtra(CATEGORY_NAME, sectionList.get(row).get(TAG_NAME));
				i.putExtra(TAG_ID, sectionList.get(row).get(TAG_ID));
				i.putExtra("token", mToken);
				startActivity(i);
			}
		});

		return list;
	}

	private void getArrayCategoriesAdapterFormatJSON(String jsonStr) {
		Log.d("Response: ", "> " + jsonStr);

		if (jsonStr != null) {
			try {
				JSONObject jsonObj = new JSONObject(jsonStr);
				String lastCategoryGroupNames = "";
				// String lastCategoryGroupId = "";

				int sizeSection = 0;

				// Getting JSON Array node
				categories = jsonObj.getJSONArray(TAG_CATEGORIES);

				List<Map<String, String>> listSeccion = new ArrayList<Map<String, String>>();

				// looping through All Categories
				for (int i = 0; i < categories.length(); i++) {
					JSONObject jsonItem = categories.getJSONObject(i);

					String id = jsonItem.getString(TAG_ID);
					String name = jsonItem.getString(TAG_NAME);
					String description = jsonItem.getString(TAG_DESCRIPTION);

					HashMap<String, String> categoryItem = new HashMap<String, String>();
					categoryItem.put(TAG_ID, id);
					categoryItem.put(TAG_NAME, name);
					categoryItem.put(TAG_DESCRIPTION, description);
					// categoryItem.put("isHeaderList", "true");
					
					if (! jsonItem.isNull(TAG_CATEGORY_GROUP)) {
						//jsonItem.isNull(name) - jsonItem.has("my_object_name")
						JSONObject categoryGroupItem = jsonItem
								.getJSONObject(TAG_CATEGORY_GROUP);
						String currentCategoryGroup_name = categoryGroupItem
								.getString(TAG_CATEGORY_GROUP_NAME);
						if (lastCategoryGroupNames
								.equals(currentCategoryGroup_name)) {
							listSeccion.add(categoryItem);
						} else {
							listSeccion = new ArrayList<Map<String, String>>();
							categoryGroupList.put(String.valueOf(sizeSection),
									listSeccion);
							categoryGroupNames.put(String.valueOf(sizeSection),
									currentCategoryGroup_name);
							listSeccion.add(categoryItem);
							lastCategoryGroupNames = currentCategoryGroup_name;
							sizeSection++;
						}
					}
				}

			} catch (JSONException e) {
				e.printStackTrace();
			}
		} else {
			Log.e("ServiceHandler", "Couldn't get any data from the url");
		}
	}
}
