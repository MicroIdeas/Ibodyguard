package com.innova.citylink.login;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.innova.citylink.MainActivity;
import com.innova.citylink.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Activity which displays a login screen to the user, offering registration as
 * well.
 */
public class RegisterActivity extends FragmentActivity {

	// TODO: pasar esto a config.xml
	private String URL = "http://citylink.leadprogrammer.com.ar/rest/user/";

	private static final String CONTENT_TYPE = "Content-Type";
	private static final String CONTENT_TYPE_VALUE = "application/json";

	private static final String PASSWORD = "password";
	private static final String USER = "user";
	private static final String EMAIL_ADDRESS = "emailAddress";
	private static final String FIRST_NAME = "firstName";
	private static final String LAST_NAME = "lastName";

	/**
	 * Keep track of the login task to ensure we can cancel it if requested.
	 */
	private UserLoginTask mAuthTask = null;

	// Values for email and password at the time of the login attempt.
	private String mFirstName;
	private String mLastName;
	private String mEmail;
	private String mPassword;
	private String mPasswordRepeat;
	private String mUserId;
	private String mToken;

	// UI references.
	private EditText mNameView;
	private EditText mEmailView;
	private EditText mPasswordView;
	private EditText mPasswordRepeatView;
	private View mLoginFormView;
	private View mLoginStatusView;
	private TextView mLoginStatusMessageView;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_register);
		
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}

		// Set up the login form.
		mNameView = (EditText) findViewById(R.id.register_name);
		mEmailView = (EditText) findViewById(R.id.register_email);
		
		mPasswordView = (EditText) findViewById(R.id.register_password);
		mPasswordView
				.setOnEditorActionListener(new TextView.OnEditorActionListener() {
					@Override
					public boolean onEditorAction(TextView textView, int id,
							KeyEvent keyEvent) {
						if (id == R.id.login || id == EditorInfo.IME_NULL) {
							register();
							return true;
						}
						return false;
					}
				});
		
		mPasswordRepeatView = (EditText) findViewById(R.id.register_password_repeat);

		mLoginFormView = findViewById(R.id.login_form);
		mLoginStatusView = findViewById(R.id.login_status);
		mLoginStatusMessageView = (TextView) findViewById(R.id.login_status_message);

		findViewById(R.id.sign_in_button).setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						register();
						hideKeyboard();
					}
				});
	}

//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		super.onCreateOptionsMenu(menu);
//		getMenuInflater().inflate(R.menu.login, menu);
//		return true;
//	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			NavUtils.navigateUpTo(this, new Intent(this,
					LoginActivity.class));
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	/**
	 * Attempts to sign in or register the account specified by the login form.
	 * If there are form errors (invalid email, missing fields, etc.), the
	 * errors are presented and no actual login attempt is made.
	 */
	public void register() {
		if (mAuthTask != null) {
			return;
		}

		// Reset errors.
		mNameView.setError(null);
		mEmailView.setError(null);
		mPasswordView.setError(null);
		mPasswordRepeatView.setError(null);

		// Store values at the time of the login attempt.
		String srtTmp = mNameView.getText().toString().trim();
		int indexOf = srtTmp.indexOf(" ");
		if(indexOf != -1) {
			mFirstName = srtTmp.substring(0, indexOf);
			mLastName = srtTmp.substring(indexOf + 1, srtTmp.length());
		} else {
			mFirstName = srtTmp;
		}

		mEmail = mEmailView.getText().toString();
		mPassword = mPasswordView.getText().toString();
		mPasswordRepeat = mPasswordRepeatView.getText().toString();

		boolean cancel = false;
		View focusView = null;
		
		// Check for a valid first name.
		if (TextUtils.isEmpty(mFirstName)) {
			mNameView.setError(getString(R.string.error_field_required));
			focusView = mNameView;
			cancel = true;
		}
//		else if (mFirstName.length() < 3) {
//			mNameView.setError(getString(R.string.error_invalid_password));
//			focusView = mNameView;
//			cancel = true;
//		}

		// Check for a valid password.
		if (TextUtils.isEmpty(mPassword)) {
			mPasswordView.setError(getString(R.string.error_field_required));
			focusView = mPasswordView;
			cancel = true;
		} else if (mPassword.length() < 4) {
			mPasswordView.setError(getString(R.string.error_invalid_password));
			focusView = mPasswordView;
			cancel = true;
		} else if (! mPassword.equals(mPasswordRepeat)) {
			mPasswordRepeatView.setError(getString(R.string.error_mismatch_password));
			focusView = mPasswordRepeatView;
			mPasswordRepeatView.setText("");
			cancel = true;
		}

		// Check for a valid email address.
		if (TextUtils.isEmpty(mEmail)) {
			mEmailView.setError(getString(R.string.error_field_required));
			focusView = mEmailView;
			cancel = true;
		} else if (!mEmail.contains("@")) {
			mEmailView.setError(getString(R.string.error_invalid_email));
			focusView = mEmailView;
			cancel = true;
		}

		if (cancel) {
			// There was an error; don't attempt login and focus the first
			// form field with an error.
			focusView.requestFocus();
		} else {
			// Show a progress spinner, and kick off a background task to
			// perform the user login attempt.
			mLoginStatusMessageView.setText(R.string.login_progress_signing_in);
			showProgress(true);
			mAuthTask = new UserLoginTask();
			mAuthTask.execute((Void) null);
		}
	}

	/**
	 * Shows the progress UI and hides the login form.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	private void showProgress(final boolean show) {
		// On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
		// for very easy animations. If available, use these APIs to fade-in
		// the progress spinner.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			int shortAnimTime = getResources().getInteger(
					android.R.integer.config_shortAnimTime);

			mLoginStatusView.setVisibility(View.VISIBLE);
			mLoginStatusView.animate().setDuration(shortAnimTime)
					.alpha(show ? 1 : 0)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							mLoginStatusView.setVisibility(show ? View.VISIBLE
									: View.GONE);
						}
					});

//			mLoginFormView.setVisibility(View.VISIBLE);
//			mLoginFormView.animate().setDuration(shortAnimTime)
//					.alpha(show ? 0 : 1)
//					.setListener(new AnimatorListenerAdapter() {
//						@Override
//						public void onAnimationEnd(Animator animation) {
//							mLoginFormView.setVisibility(show ? View.GONE
//									: View.VISIBLE);
//						}
//					});
		} else {
			// The ViewPropertyAnimator APIs are not available, so simply show
			// and hide the relevant UI components.
			mLoginStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
			mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
		}
	}

	/**
	 * Represents an asynchronous login/registration task used to authenticate
	 * the user.
	 */
	public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

		@Override
		protected Boolean doInBackground(Void... params) {
			boolean resul = true;

			HttpClient httpClient = new DefaultHttpClient();
			HttpPost post = new HttpPost(URL);
			post.setHeader(CONTENT_TYPE, CONTENT_TYPE_VALUE);

			try {
				JSONObject userData = new JSONObject();
				userData.put(FIRST_NAME, mFirstName);
				userData.put(LAST_NAME, mLastName);
				userData.put(EMAIL_ADDRESS, mEmail);

				JSONObject jsonUser = new JSONObject();
				jsonUser.put(USER, userData);
				jsonUser.put(PASSWORD, mPassword);

				StringEntity entity = new StringEntity(jsonUser.toString());
				post.setEntity(entity);

				HttpResponse resp = httpClient.execute(post);
				String respStr = EntityUtils.toString(resp.getEntity());

				parseJSON(respStr);

			} catch (Exception ex) {
				Log.e("ServicioRest", "Error!", ex);
				resul = false;
			}

			return resul;
		}

		private void parseJSON(String jsonStr) throws JSONException {
			JSONObject jsonObj = new JSONObject(jsonStr);
			if (jsonStr != null) {
				mToken = jsonObj.get("token").toString();
				mUserId = jsonObj.get("userId").toString();
			}
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			mAuthTask = null;
			showProgress(false);

			if (success) {
				Toast.makeText(getApplicationContext(), "Bienvenido :)",
						Toast.LENGTH_LONG).show();

				Intent successIntent = new Intent(getApplicationContext(),
						MainActivity.class);
				successIntent.putExtra("userId", mUserId);
				successIntent.putExtra("mToken", mToken);

				startActivity(successIntent);

				// finish();
			} else {
				mPasswordView
						.setError(getString(R.string.error_incorrect_password));
				mPasswordView.requestFocus();
			}
		}

		@Override
		protected void onCancelled() {
			mAuthTask = null;
			showProgress(false);
		}
	}
	
	private void hideKeyboard() {
		// Lineas para ocultar el teclado virtual (Hide keyboard)
		InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
	    imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
	}

	// LOGIN CON GOOGLE VOLLEY
	// Benchmark performance:
	// volley:
	// 04-26 20:17:57.605: I/System.out(19778): El tiempo del proceso total es
	// :1312 miliseg
	// 04-26 20:19:57.275: I/System.out(20270): El tiempo del proceso total es
	// :1030 miliseg
	// 04-26 20:20:38.675: I/System.out(20358): El tiempo del proceso total es
	// :1208 miliseg
	// 04-26 20:21:14.940: I/System.out(20442): El tiempo del proceso total es
	// :1744 miliseg
	// 04-26 20:26:25.320: I/System.out(21598): El tiempo del proceso total es
	// :1381 miliseg
	//
	// HttpClient:
	// 04-26 20:24:51.070: I/System.out(20954): El tiempo del proceso total es
	// :756 miliseg
	// 04-26 20:25:35.390: I/System.out(21405): El tiempo del proceso total es
	// :843 miliseg
	// 04-26 20:27:02.735: I/System.out(22064): El tiempo del proceso total es
	// :714 miliseg
	// private void login() {
	// final long tiempoInicio = System.currentTimeMillis();
	//
	// String URL = "http://citylink.leadprogrammer.com.ar/rest/user/";
	// RequestQueue queue = Volley.newRequestQueue(this);
	//
	// // // Post params to be sent to the server
	// // Map<String, String> params = new HashMap<String, String>();
	// // params.put("firstName", "Name" + mEmail);
	// // params.put("lastName", "LastName" + mEmail);
	// // params.put("emailAddress", mEmail);
	//
	// JSONObject datos = new JSONObject();
	// try {
	// datos.put("firstName", "Name" + mEmail);
	// datos.put("lastName", "LastName" + mEmail);
	// datos.put("emailAddress", mEmail);
	// } catch (JSONException e1) {
	// e1.printStackTrace();
	// }
	//
	// JSONObject user2 = new JSONObject();
	// try {
	// user2.put("user", datos);
	// user2.put("password", mPassword);
	// } catch (JSONException e1) {
	// e1.printStackTrace();
	// }
	//
	// JsonObjectRequest req = new JsonObjectRequest(URL, user2,
	// new Response.Listener<JSONObject>() {
	// @Override
	// public void onResponse(JSONObject response) {
	// try {
	// // VolleyLog.v("Response:%n %s",
	// // response.toString(4));
	// // System.out.println("response 1 " +
	// // response.toString(4));
	// // System.out.println("response 2 " +
	// // response.toString());
	// // response1 = response;
	// mToken = response.get("token").toString();
	// mUserId = response.get("userId").toString();
	//
	// long totalTiempo = System.currentTimeMillis()
	// - tiempoInicio;
	//
	// System.out
	// .println("El tiempo del proceso total es :"
	// + totalTiempo + " miliseg");
	//
	// showProgress(false);
	// // Toast.makeText(getApplicationContext(), "mUserId"
	// // + mUserId + "mToken: " + mToken,
	// // Toast.LENGTH_LONG).show();
	//
	// Intent i = new Intent(getApplicationContext(),
	// AppMainActivity.class);
	// i.putExtra("userId", mUserId);
	//
	// startActivity(i);
	//
	// } catch (JSONException e) {
	// e.printStackTrace();
	// }
	// }
	// }, new Response.ErrorListener() {
	// @Override
	// public void onErrorResponse(VolleyError error) {
	// VolleyLog.e("Error: ", error.getMessage());
	// }
	// });
	//
	// // add the request object to the queue to be executed
	// queue.add(req);
	//
	// }
}