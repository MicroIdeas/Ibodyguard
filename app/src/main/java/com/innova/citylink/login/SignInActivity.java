package com.innova.citylink.login;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.innova.citylink.MainActivity;
import com.innova.citylink.R;
import com.innova.citylink.login.sessions.UserSessionManager;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Activity which displays a login screen to the user, offering registration as
 * well.
 */
public class SignInActivity extends FragmentActivity {
	/**
	 * Keep track of the login task to ensure we can cancel it if requested.
	 */
	private UserLoginTask mAuthTask = null;
	private CategoryTask mCategoryTask = null;
	private String mUserId;
	private String mToken;

	// Values for email and password at the time of the login attempt.
	private String mEmail;
	private String mPassword;

	// UI references.
	private EditText mEmailView;
	private EditText mPasswordView;
	private View mLoginFormView;
	private View mLoginStatusView;
	private TextView mLoginStatusMessageView;
	private TextView mPasswordLost;

	// User Session Manager Class
	UserSessionManager session;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_sign_in);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}

		// User Session Manager
		session = new UserSessionManager(getApplicationContext());

		// Set up the login form.
		mEmailView = (EditText) findViewById(R.id.email);
		mEmailView.setText(mEmail);

		mPasswordView = (EditText) findViewById(R.id.password);
		mPasswordView
				.setOnEditorActionListener(new TextView.OnEditorActionListener() {
					@Override
					public boolean onEditorAction(TextView textView, int id,
							KeyEvent keyEvent) {
						if (id == R.id.login || id == EditorInfo.IME_NULL) {
							login();
							return true;
						}
						return false;
					}
				});

		mLoginFormView = findViewById(R.id.login_form);
		mLoginStatusView = findViewById(R.id.login_status);
		mLoginStatusMessageView = (TextView) findViewById(R.id.login_status_message);

		findViewById(R.id.sign_in_button).setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						login();
						hideKeyboard();
					}
				});

		mPasswordLost = (TextView) findViewById(R.id.txt_password_lost);
		mPasswordLost.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Toast.makeText(getApplicationContext(), "No implementado.",
						Toast.LENGTH_SHORT).show();
			}
		});

	}

	// @Override
	// public boolean onCreateOptionsMenu(Menu menu) {
	// super.onCreateOptionsMenu(menu);
	// getMenuInflater().inflate(R.menu.sign_in, menu);
	// return true;
	// }

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			NavUtils.navigateUpTo(this, new Intent(this, LoginActivity.class));
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	/**
	 * Attempts to sign in or register the account specified by the login form.
	 * If there are form errors (invalid email, missing fields, etc.), the
	 * errors are presented and no actual login attempt is made.
	 */
	public void login() {
		if (mAuthTask != null) {
			return;
		}

		// Reset errors.
		mEmailView.setError(null);
		mPasswordView.setError(null);

		// Store values at the time of the login attempt.
		mEmail = mEmailView.getText().toString();
		mPassword = mPasswordView.getText().toString();

		boolean cancel = false;
		View focusView = null;

		// Check for a valid password.
		if (TextUtils.isEmpty(mPassword)) {
			mPasswordView.setError(getString(R.string.error_field_required));
			focusView = mPasswordView;
			cancel = true;
		} else if (mPassword.length() < 4) {
			mPasswordView.setError(getString(R.string.error_invalid_password));
			focusView = mPasswordView;
			cancel = true;
		}

		// Check for a valid email address.
		if (TextUtils.isEmpty(mEmail)) {
			mEmailView.setError(getString(R.string.error_field_required));
			focusView = mEmailView;
			cancel = true;
		} else if (!mEmail.contains("@")) {
			mEmailView.setError(getString(R.string.error_invalid_email));
			focusView = mEmailView;
			cancel = true;
		}

		if (cancel) {
			// There was an error; don't attempt login and focus the first
			// form field with an error.
			focusView.requestFocus();
		} else {
			// Show a progress spinner, and kick off a background task to
			// perform the user login attempt.
			mLoginStatusMessageView.setText(R.string.login_progress_signing_in);
			showProgress(true);

			mAuthTask = new UserLoginTask();
			mAuthTask.execute((Void) null);
		}
	}

	private void loadCategories() {
		if (mCategoryTask != null) {
			return;
		}

		boolean loginOk = true;

		if (loginOk) {
			mLoginStatusMessageView.setText("Actualizando datos... (2/2)");
			showProgress(true);

			mCategoryTask = new CategoryTask();
			mCategoryTask.execute((Void) null);
		} else {

		}
	}

	/**
	 * Shows the progress UI and hides the login form.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	private void showProgress(final boolean show) {
		// On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
		// for very easy animations. If available, use these APIs to fade-in
		// the progress spinner.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			int shortAnimTime = getResources().getInteger(
					android.R.integer.config_shortAnimTime);

			mLoginStatusView.setVisibility(View.VISIBLE);
			mLoginStatusView.animate().setDuration(shortAnimTime)
					.alpha(show ? 1 : 0)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							mLoginStatusView.setVisibility(show ? View.VISIBLE
									: View.GONE);
						}
					});

//			mLoginFormView.setVisibility(View.VISIBLE);
//			mLoginFormView.animate().setDuration(shortAnimTime)
//					.alpha(show ? 0 : 1)
//					.setListener(new AnimatorListenerAdapter() {
//						@Override
//						public void onAnimationEnd(Animator animation) {
//							mLoginFormView.setVisibility(show ? View.GONE
//									: View.VISIBLE);
//						}
//					});
		} else {
			// The ViewPropertyAnimator APIs are not available, so simply show
			// and hide the relevant UI components.
			mLoginStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
			mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
		}
	}

	/**
	 * Represents an asynchronous login/registration task used to authenticate
	 * the user.
	 */
	public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

		@Override
		protected Boolean doInBackground(Void... params) {

			HttpClient httpClient = new DefaultHttpClient();
			HttpPost post = new HttpPost(LoginConstant.URL_SIGN_IN);
			post.setHeader(LoginConstant.CONTENT_TYPE,
					LoginConstant.CONTENT_TYPE_VALUE);

			try {
				JSONObject userLogin = new JSONObject();
				userLogin.put(LoginConstant.USERNAME, mEmail);
				userLogin.put(LoginConstant.PASSWORD, mPassword);

				StringEntity entity = new StringEntity(userLogin.toString());
				post.setEntity(entity);

				HttpResponse resp = httpClient.execute(post);
				String respStr = EntityUtils.toString(resp.getEntity());

				return login(respStr);
			} catch (Exception ex) {
				Log.e("ServicioRest", "Error!", ex);
				return false;
			}
		}

		private boolean login(String jsonStr) throws JSONException {
			JSONObject jsonObj = new JSONObject(jsonStr);

			if (jsonStr != null && jsonObj.has("token")) {
				mToken = jsonObj.get("token").toString();
				mUserId = jsonObj.get("userId").toString();
			} else {
				Log.d("ServicioRestLogin", "Error Login");
				return false;
			}

			return !mToken.equals("") && mToken.length() > 1
					&& !mUserId.equals("") && mUserId.length() > 1;
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			mAuthTask = null;
			showProgress(false);

			if (success) {
				loadCategories();
			} else {
				mPasswordView
						.setError(getString(R.string.error_incorrect_password));
				mPasswordView.requestFocus();
			}
		}

		@Override
		protected void onCancelled() {
			mAuthTask = null;
			showProgress(false);
		}
	}

	public class CategoryTask extends AsyncTask<Void, Void, Boolean> {

		private String response = null;

		@Override
		protected Boolean doInBackground(Void... params) {
			HttpClient httpClient = new DefaultHttpClient();
			HttpGet httpGet = new HttpGet(LoginConstant.URL_CATEGORY);

			try {
				HttpResponse resp = httpClient.execute(httpGet);
				response = EntityUtils.toString(resp.getEntity(), HTTP.UTF_8);

				return response != null && response.length() > 0;
			} catch (Exception ex) {
				Log.e("CategoryTask Exception", "Error!", ex);
				return false;
			}
		}

		@Override
		protected void onPostExecute(Boolean success) {
			mAuthTask = null;
			showProgress(false);

			if (success) {
				FileOutputStream fos;
				try {
					deleteFile(LoginConstant.CATEGORIES_JSON);

					fos = openFileOutput(LoginConstant.CATEGORIES_JSON,
							Context.MODE_PRIVATE);
					fos.write(response.getBytes());
					fos.close();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}

				Toast.makeText(getApplicationContext(), "Bienvenido :)",
						Toast.LENGTH_LONG).show();

				// Creating user login session
				// Statically storing:
				session.createUserLoginSession(mPassword, mEmail, mToken);

				Intent successIntent = new Intent(getApplicationContext(),
						MainActivity.class);
				successIntent.putExtra("userId", mUserId);
				successIntent.putExtra("token", mToken);

				startActivity(successIntent);
			} else {
				// TODO error. volver a intentar una vez mas. si falla comprobar
				// que el file json tiene datos y usar ese.
			}
		}

		@Override
		protected void onCancelled() {
			mCategoryTask = null;
			showProgress(false);
		}
	}
	
	private void hideKeyboard() {
		// Lineas para ocultar el teclado virtual (Hide keyboard)
		InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
	    imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
	}
}
