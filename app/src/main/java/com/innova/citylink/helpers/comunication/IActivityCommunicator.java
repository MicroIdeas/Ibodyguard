package com.innova.citylink.helpers.comunication;

import com.google.android.gms.maps.model.LatLng;

import org.apache.http.entity.mime.content.ByteArrayBody;

public interface IActivityCommunicator {

	public void setStreetLocation(String streetLocationValue);

	public void setLatLng(LatLng latLngValue);

	public void setPhotoReport(ByteArrayBody byteArrayBodyValue);

}
